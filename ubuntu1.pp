# node configuration for ubuntu1
package { [ 'apache2','curl' ]: ensure => present, }

file { '/var/www/html/index.html':
  ensure  => present,
  owner   => 'root',
  group   => 'www-data',
  mode    => '0644',
  content => "This is a webserver running at ${::hostname}",
  require => Package['apache2'],
}

service { 'apache2':
  ensure  => running,
  enable  => true,
  require => Package['apache2'],
}


